/*
---------------------------
DEFINITIONS
---------------------------
Define Global Variables for Sheets and Ranges
--------------------------
*/
var recruiter_sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName('Recruiter');
var search_results_sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName('SearchResults');  
var data_sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName('Data');
var last_data_row = data_sheet.getLastRow();
var data_sheet_range = data_sheet.getRange('A4:G'+last_data_row).getValues();
var canvass_tracking = SpreadsheetApp.openById('1qLIDe69jev5Tj1A-tEuMsvvlxIagXBdqu8OtDgvt3OA');
var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
/*
---------------------------
FUNCTION: getRandomArbitrary
---------------------------
takes integers representing minimum and maximum and generates a random float between them.
ARGS: float
RETURNS: float
---------------------------
*/  
function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}
/*
---------------------------
FUNCTION: random_person
---------------------------
creates a random entry for the data results for testing purposes
ARGS: none
RETURNS: object
---------------------------
*/ 
function random_person() {
  var first_names= ["Aaron", "Adam", "Amy", "Amanda", "Agnus", "Anne", "Brittany", "Bob", "Brandon", "Brandy", "Brynn", "Belle",
                    "Cary", "Carl", "Cam", "Cordero", "Calvin", "Cindy", "Celia", "Devin", "David", "Danny", "Dillon", "Danna", "Erin",
                    "Emily", "Ed", "Edwardo", "Edgar", "Emmet", "Freddy", "Frederick", "Frances", "Francois", "Frank", "Gabby", "Gerard",
                    "Gwen", "Harry", "Harold", "Harriet", "Hamish", "Ian", "Ingrid", "Justin", "Jeremy", "Jacob", "Janet", "Janice", 
                    "Jordon", "Jordy", "Justice", "Korin", "Kevin", "Luci", "Lucy", "Landon", "Marsha", "Mary", "Maggie", "Mo", "Marvin",
                    "Nanci", "Nathan", "Nathaniel","Orlando", "Orval", "Peter", "Randy", "Renee", "Shawna", "Sean", "Sally", "Sam", 
                    "Trevor", "Tina", "Ulga", "Victoria", "Victor", "Vicky", "Wendy", "Winston"];
  var middle_names = ["Aaron", "Adam", "Amy", "Amanda", "Agnus", "Anne", "Brittany", "Bob", "Brandom", "Brandy", "Brynn", "Belle",
                      "Cary", "Carl", "Cam", "Cordero", "Calvin", "Cindy", "Celia", "Devin", "David", "Danny", "Dillon", "Danna", "Erin",
                      "Emily", "Ed", "Edwardo", "Edgar", "Emmet", "Freddy", "Frederick", "Frances", "Francois", "Frank", "Gabby", "Gerard",
                      "Gwen", "Harry", "Harold", "Harriet", "Hamish", "Ian", "Ingrid", "Justin", "Jeremy", "Jacob", "Janet", "Janice", 
                      "Jordon", "Jordy", "Justice", "Korin", "Kevin", "Luci", "Lucy", "Landon", "Marsha", "Mary", "Maggie", "Mo", "Marvin",
                      "Nanci", "Nathan", "Nathaniel","Orlando", "Orval", "Peter", "Randy", "Renee", "Shawna", "Sean", "Sally", "Sam", 
                      "Trevor", "Tina", "Ulga", "Victoria", "Victor", "Vicky", "Wendy", "Winston"];
  var last_names= ["Appleton", "Albreckt", "Adams", "Brunswick", "Fairibottom", "Majors", "Thornton", "Dixon", "Johns", "Cooper","Flintstone",
                   "Babbleton", "Wellingbottom", "Wipdeedoo", "Wondershow", "Wixywick", "Dellingsnap", "Dramallama", "Fellowtree", "Clamsburg",
                   "Harpysong", "Merrymixer", "Marvinshodd", "Glee", "Mustache", "Trix", "Secretstain", "Brokenbellow", "Peartree", "Frederickson",
                   "Fancypants", "Farlington", "Peestone", "Flatrock", "Mutton", "Hambaker", "Elmosbile"];
  
  this.firstName = first_names[Math.round(getRandomArbitrary(0,first_names.length-1))];
  this.middleName = middle_names[Math.round(getRandomArbitrary(0,middle_names.length-1))];
  this.lastName = last_names[Math.round(getRandomArbitrary(0,last_names.length-1))];
  this.raised = Math.round(getRandomArbitrary(10,35))*10;
  this.recruiter = "nobody";  
}
/*
---------------------------
FUNCTION: dummyData
---------------------------
creates and writes the given number of random data entries
ARGS: int
RETURNS: none
---------------------------
*/ 
function dummyData(int){
  int = 1000
  var monthlyAmounts = [15, 15, 15, 15, 20, 25, 30]
  var recruiters= ["Lawrence Weill",
                   "Kevin O'Connor",
                   "Warren Williams",
                   "Caz Kanabrocki",
                   "Ace (D'Angelo) LaGrone",
                   "Jacob Mccurry",
                   "Sloan Klusendorf",
                   "Ryan O'Malley"]
  var newSignup = [];
  var newData = []; 
  for(i=0; i<int; i++){
    var person = new random_person;
    var month = getRandomArbitrary(0,11);
    var last_day = new Date(2017, month+1, 0).getDate();
    var day = Math.round(getRandomArbitrary(0,last_day));
    var signup_date = new Date(2017, month+1, day);
    var amount = monthlyAmounts[Math.round(getRandomArbitrary(0,monthlyAmounts.length-1))]
    newSignup.push(signup_date);
    newSignup.push(person.firstName+" "+person.middleName+" "+person.lastName);
    newSignup.push(amount)
    newSignup.push("Monthly");
    newSignup.push(amount*8);
    newSignup.push("");
    newSignup.push(recruiters[Math.round(getRandomArbitrary(0,recruiters.length-1))])
    newData.push(newSignup);
    newSignup = [];
  }; 
  data_sheet.getRange("A4:G1003").setValues(newData)
};

