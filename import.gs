/*
----------------------
FUNCTION: importDaily
----------------------
Grab the data from the daily export. Loop through each row
and within each row perform the following in sequence:
1) Push the date into the export row
2) Push the amount raised into the export row
3) Push the combined [salutation][first][middle][last] name into the export row
4) Push the donation type into the export row
5) Push the cancellation status into the export row
6) push the recruiter into the export row
this should leave the export row with the following data in order:
[DATE,RAISED,MEMBERNAME,DONATIONTYPE,CANCELLATIONSTATUS,RECRUITER]
*/
function importDaily(){
  //Open the daily import staging sheet
  var DailyImport_Sheet = SpreadsheetApp.openById('1PXewhrDISwFfNot2U9If2vhI2QsTRyhVy0VAxLc0DOg').getSheetByName('Daily_Import');
  var lastRow = DailyImport_Sheet.getLastRow();
  var DailyImport_Range = DailyImport_Sheet.getRange('A2:AI'+lastRow); 
  var DailyImport_Data = DailyImport_Range.getValues();
  var output_row = [];
  var output_array = []; 
  //Open Prompt to Confirm
  var ui = SpreadsheetApp.getUi();
  var affirm = ui.alert("Import", "are you sure you wish to import?", ui.ButtonSet.OK_CANCEL);
  if (affirm != ui.Button.OK){return};
  //Define variables to make it easier to locate particular data within the array
  var date = 25;
  var donation_amount = 18;
  var salutation = 2;
  var first_name = 3;
  var middle_initial = 4;
  var last_name = 5;
  var donation_type = 19
  var recruiter = 27;    
  //Loop through the data from the results Doc (called Daily Export) 
  //Format the data for the Canvass Data sheet and place it in output_array
  for(var i=0; i<DailyImport_Data.length; i++){ 
        output_row.push(DailyImport_Data[i][date]); 
        //Grab and combine first, middle, and last names
        var name = DailyImport_Data[i][first_name]+" "+DailyImport_Data[i][middle_initial]+ " " +DailyImport_Data[i][last_name];
        //add salutation if present
        if(DailyImport_Data[i][salutation] != ""){name = name+" ("+DailyImport_Data[i][salutation]+") "};
        //Remove any extra spaces from the front of names
        while(name.charAt(0) === ' '){ name = name.substr(1)};
        output_row.push(name); 
        output_row.push(DailyImport_Data[i][donation_amount]); 
        output_row.push(DailyImport_Data[i][donation_type]);
        if(DailyImport_Data[i][donation_type]=='Monthly'){
          output_row.push(DailyImport_Data[i][donation_amount]*8)
        }
        else{output_row.push(DailyImport_Data[i][donation_amount])};  
        output_row.push(" ");//Blank for cancellation status  
        output_row.push(DailyImport_Data[i][recruiter]);        
    //Push the row and reset it 
      output_array.push(output_row);
      output_row = [];
  };
  //Find the last row of Canvass Data and select range for export
  var Data_sheet = SpreadsheetApp.openById("1WI7kD_eTLxRVqYtDrIRlp95hiJX51z0Swo9RIavDaxk").getSheetByName('data');
  var last_row = Data_sheet.getLastRow();
  var start_row = last_row+1  
  var output_range = Data_sheet.getRange(start_row, 1, output_array.length, 7);
  //Export
  Logger.log(output_array);
  output_range.setValues(output_array);  
};


