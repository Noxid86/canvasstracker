# CanvassTracker

Javascript code for a dynamic Google Sheet meant to calculate fundraising numbers for canvassers. 
Instructions for Use:

	Import Daily: the button in the top right corner pulls data from an adjacent document that the organization has updated each day.
	the 'Data' sheet stores all signups for the year as they are imported. 
	
	Recruiter: By selecting a year and month in the dropdown above the calendar and a canvasser then clicking search the script
	will tally numbers for that canvasser in the given month and create a calendar displaying their results. 
	
	Search: After a recruiter search is performed the individual signup data queried will also be displayed on this tab for
	easy reference.
	
	
Product can be viewed https://docs.google.com/spreadsheets/d/1X1f3oOD5xmuEt0XixM-7xrm9-zLuqupMP5rtto6vvek/edit?usp=sharingS