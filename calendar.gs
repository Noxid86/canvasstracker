/*
---------------------------
DEFINITIONS
---------------------------
Define Global Variables for Sheets and Ranges
--------------------------
*/
var recruiter_sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName('Recruiter');
var search_results_sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName('SearchResults');  
var data_sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName('Data');
var last_data_row = data_sheet.getLastRow();
var data_sheet_range = data_sheet.getRange('A4:G'+last_data_row).getValues();
var canvass_tracking_spreadsheet = SpreadsheetApp.openById('1SFX6uqGosO613R36MsdMEx3EBTQbnmdjCUVK2atdytU');
var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
/*
---------------------------
FUNCTION: getRandomArbitrary
---------------------------
takes integers representing minimum and maximum and generates a random float between them.
ARGS: float
RETURNS: float
---------------------------
*/  
function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}
/*
---------------------------
FUNCTION: intersperse
---------------------------
shove the given element into
every other row of the array
--------------------------
*/ 
function intersperse(arr, el) {
  var res = [], i=0;
  if (i < arr.length)
    res.push(arr[i++]);
  while (i < arr.length)
    res.push(el, arr[i++]);
  return res;
}  


/*
---------------------------
FUNCTION: printCalendarValues
---------------------------
args: a one dimmensional array where the index+1 represents the day of the month
returns: prints each day in the corresponding day of the month
--------------------------
*/ 
function printCalendarValues(array){ 
  var Calendar_Display_range = recruiter_sheet.getRange('B8:H19');
  var Calendar_Display_values = Calendar_Display_range.getValues();
  //getCellBelow - take int, return range of space below that int in the calendar
  function getCellBelow(day, rangeArray){
    for(var i=0; i<rangeArray.length; i++){
      for(j=0;j<rangeArray[i].length; j++){
        if(rangeArray[i][j]==day){
          var res = [i+9,j+2];
          return res;
        };        
      };
    }
  };  
  for(var i = 0; i<array.length; i++){
    var cellBelow = getCellBelow(i+1, Calendar_Display_values);  
    recruiter_sheet.getRange(cellBelow[0],cellBelow[1]).setValue(array[i]);
  } 
};

/*
---------------------------
FUNCTION: buildEmptyCalendar
---------------------------
Build an array of data representing all of the days 
in the month given
ARGS: Year(int), Month(int), isMap(bool)
RETURNS: Array
--------------------------
*/    
function buildEmptyCalendar(year, month, isMap){
  var isMap=isMap||false;
  var selected_month = months.indexOf(month);
  var firstDay = new Date(year,selected_month);
  var lastDay = new Date(year,selected_month+1,0);
  var daysOfMonth = [];
  var Export_Calendar = [];
  //Get the date for each day of the selected month and push it into daysOfMonth
  for(i = 0; i<lastDay.getDate();i++){
    daysOfMonth.push(new Date(year,selected_month,i+1, 5).getDate()); 
  }
  //Run the following only if isMap is true
  if(isMap){
    //push empty objects to the front and back of the array to align days with weekdays in the Calendar Display
    for(i = 0; i<firstDay.getDay(); i++){
      daysOfMonth.unshift('');
    }
    for(i = 0; i<(6-lastDay.getDay()); i++){
      daysOfMonth.push('');
    } 
    //Chunk calendar into weeks
    var week = [];
    for(i=0 ; i<daysOfMonth.length; i++ ){        
      if (week.length<6){
        week.push(daysOfMonth[i]);
      }
      else{
        week.push(daysOfMonth[i]);
        Export_Calendar.push(week);
        week = [];
      } 
    } 
    //Add empty rows for values
    var value_week = ["","","","","","",""];
    Export_Calendar = intersperse(Export_Calendar, value_week); 
    while(Export_Calendar.length<12){
      Export_Calendar.push(value_week);
    }
    //finding the index of a date's value cell
    //try indexOf Export_Calendar [i]["14"] to find the second dimensional index of the 14th in it's week
    return Export_Calendar; 
  }
  else{
    return daysOfMonth;
  }; 
}

/*
---------------------------
FUNCTION: recruiter_search
---------------------------
Search through the 'Data' sheet for any members signed up by the name in the search cell
return the array
--------------------------
*/
function recruiter_search(queried_name) {  
  //Define Convenience Variables for column labels
  var date = 0;
  var member_name = 1;
  var amount = 2;
  var type = 3;
  var raised = 4;
  var cancellation = 5;
  var recruiter = 6; 
  //Define search variables
  var recruiter_rows = [];
  //Find all rows which contain 'name' in recruiter column and store their index
  for(var i=0; i<last_data_row-3; i++){
    if (data_sheet_range[i][recruiter]==queried_name){
      recruiter_rows.push(i);     
    }    
  }  
  //Get the rows that correspond to the requested recruiter (via stored index) 
  //and put them in search_results
  var search_results = [];
  for(var i=1; i<recruiter_rows.length; i++){ 
    search_results.push(data_sheet_range[recruiter_rows[i]]);  
  };
  //Clear data from output cells
  var clear_range = search_results_sheet.getRange(2,1,search_results_sheet.getLastRow()+4,7);
  clear_range.clear();
  //pull data from each of those rows and place in current sheet   
  var end = search_results.length+1;
  writeToSearchRange(search_results);
  return search_results; 
} 

/*
---------------------------
FUNCTION: filterResults
---------------------------
Arguments: 'results' - an array from recruiter_search(), 'by' - by can be month|day|year, 'time' - a date object, 
----------------
Returns: all rows from the array with a date object that matches 'time' via .getDate
--------------------------
*/
function filterResults(results, by, year, month, day){
  var outputArray = [];  
  month = months.indexOf(month);
  var match = new Date(year,month,day);
  switch(by){
    case"day":
      for(i=0; i<results.length; i++){
        var currDate = results[i][0];
        if (currDate.getDate() == match.getDate && currDate.getMonth() == match.getMonth && currDate.getYear() == match.getYear()){      
          outputArray.push(results[i]);
        };
      };
      return outputArray;
    case"month":
      for(i=0; i<results.length; i++){
        var currDate = results[i][0];
        var type = typeof currDate;
        if (currDate.getMonth() == match.getMonth()&&currDate.getYear() == match.getYear()){    
          outputArray.push(results[i]);
        };
      };    
      return outputArray;
    case"year":
      for(i=0; i<results.length; i++){
        var currDate = results[i][0];
        if (currDate.getYear() == match.getYear()){      
          outputArray.push(results[i]);
        };
      };  
      return outputArray;
  }  
};
/*
---------------------------
FUNCTION: createMonthlyData
---------------------------
takes an array of canvasser data and compiles the total raised for each day
--------------------------
*/
//STEP ONE: Create output map that represents dates of the given month
//STEP TWO: Go through each 'day' of the calendar map and search the given data for corresponding dates
//STEP THREE: Repeat steps one and two with canvasser data
//STEP FOUR: Reconcile the two maps
//STEP GIVE: assign the values from the mapped array to the corresponding calendar date spaces

function createMonthlyData(year,month,canvasser){ 
  //--sub function for comparing an array of values to the calendar and summing them
  function findAllIn(the_year,the_month,the_canvasser,doc){
    var dataSumMap = [];
    var dataMonthMap = buildEmptyCalendar(year, month); 
    var canvasser_data = recruiter_search(canvasser);
    canvasser_data = filterResults(canvasser_data,"month", the_year, the_month, 1);  
    var claimed_data_sheet = canvass_tracking_spreadsheet.getSheetByName(the_canvasser);
    var claimed_LastRow = claimed_data_sheet.getLastRow();
    var claimed_data = claimed_data_sheet.getRange('A2:P366').getValues();
    claimed_data = filterResults(claimed_data, "month", the_year, the_month, 1); 
    
    var searchArray = [];
    //Adjust variables depending on the document being compared
    if(doc!="tracking"){
      searchArray = canvasser_data;
      var raised = 4;
      var date = 0;
    }
    else{
      searchArray = claimed_data
      var raised = 10;
      var date = 0;
    };
    for(i=0;i<dataMonthMap.length;i++){     
      var like_values = [];
      function add(a, b) { 
        return a + b;
      }
      for(j=0;j<searchArray.length;j++){ 
        if(searchArray[j][date].getDate()==dataMonthMap[i]){
          like_values.push(searchArray[j][raised]);               
        } 
      }; 
      like_values = like_values.reduce(add, 0);
      dataSumMap.push(like_values);     
    };
    return dataSumMap
  };
  /*
  ---------------------------
  SUB-FUNCTION: reconcileMonthlyData
  ---------------------------
  Arguments: claimed_array - 2d array, data_array - 2d array
  ----------------
  Returns: an array of all matching values and values that dont match concatated with the corresponding < or >
  --------------------------
  */
  function reconcileMonthlyData(claimed_array, data_array){
    var reconciledData = [];
    for(i=0;i<claimed_array.length;i++){
      if(claimed_array[i]==data_array[i]){
        reconciledData.push(data_array[i]);
      }
      else{
        if(claimed_array[i]<data_array[i]){reconciledData.push(claimed_array[i]+"<"+data_array[i])}
        else{reconciledData.push(claimed_array[i]+">"+data_array[i])};        
      };
    };
    return reconciledData;
  };
  //Gather the amount raised for each day from the Canvass Tracking form
  var claimed_sheet = canvass_tracking_spreadsheet.getSheetByName(canvasser);
  var claimed_data = claimed_sheet.getRange('A2:P'+claimed_sheet.getLastRow());
  //Gather the amount raised for each day from the 2017 Canvass Data sheet and the Claimed Data
  var data_map = findAllIn(year,month,canvasser,'data');
  var claimed_map = findAllIn(year,month,canvasser,'tracking');
  return reconcileMonthlyData(claimed_map,data_map);
};
/*
---------------------------
FUNCTION: writeCalendar
---------------------------
takes an array of calendar data and writes it to the range representing the calendar
--------------------------
*/
function writeCalendar(calendarData){
  var Calendar_Display_range = recruiter_sheet.getRange('B8:H19');
  Calendar_Display_range.setValues(calendarData);  
};
/*
---------------------------
FUNCTION: writeToSearchRange
---------------------------
takes an array of canvasser data and prints it to the search results range
--------------------------
*/
function writeToSearchRange(array){
  var search_results_data_range = search_results_sheet.getRange('A2:G'+((array.length)+1));
  search_results_data_range.setValues(array);
};
/*
---------------------------
SPREADSHEET INTERACTIONS
---------------------------

--------------------------
*/
function search(){
  var selected_recruiter = recruiter_sheet.getRange('A2').getValue();
  recruiter_search(selected_recruiter);
  var selected_month = recruiter_sheet.getRange('D6').getValue();
  var selected_year = recruiter_sheet.getRange('G6').getValue();
  var current_monthly_data = buildEmptyCalendar(selected_year, selected_month, true);   
  var monthlyData = createMonthlyData(selected_year, selected_month, selected_recruiter);
  writeCalendar(current_monthly_data);  
  printCalendarValues(monthlyData)
};




